import java.util.*;
public class MagicSquare {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows: ");
        int row = sc.nextInt();
        System.out.print("Enter the number of columns: ");
        int col = sc.nextInt();
        int[][] arr = new int[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                arr[i][j] = sc.nextInt();
            }
        }
        int count=0,k=0;
        int[] a=new int[(2*row)+2];
        for(int i=0;i<row;i++)
        {
            int rowSum=0;
            for(int j=0;j<col;j++)
            {
                rowSum+=arr[i][j];
            }
            a[count]=rowSum;
            count++;
        }
        for(int i=0;i<row;i++)
        {
            int colSum=0;
            for(int j=0;j<col;j++)
            {
                colSum+=arr[j][k];

            }
            a[count]=colSum;
            count++;
            k++;
        }
        int forwDiagSum=0;
        for(int i=0;i<row;i++)
        {
            forwDiagSum+=arr[i][i];
        }
        a[count]=forwDiagSum;
        count++;
        int l=row-1,backDiagSum=0;
        for(int i=0;i<row;i++)
        {
            backDiagSum+=arr[i][l];
            l--;
        }
        a[count]=backDiagSum;
        //check first element with all the elements of array.If same ,will print yes else print no
        int firstEle=a[0],flag=0;
        for(int i=1;i<a.length;i++)
        {
            if(firstEle!=a[i]) {
                System.out.println("no");
                flag=1;
                break;
            }
        }
        if(flag==0)
            System.out.println("yes");


    }





}



